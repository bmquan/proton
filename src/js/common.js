jQuery(document).ready(function($) {
	MenuButton();
});

function MenuButton() {
	$('.menu-btn').on("click", function(e){
        e.stopPropagation();
        $('.menu-btn').toggleClass('is-active');
        $('.header__gnavi').toggleClass('is-open').stop().slideToggle();
    }); 
}
