<?php
/* Template name: Vị trí */
$thisPageName = 'vi-tri';
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/location.min.css">
</head>
<body id="location" class='location subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<ul class="breadcrum">
								<li><a href="<?php echo APP_ASSETS; ?>">Trang chủ</a></li>
								<li>Vị trí</li>
							</ul>
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post();
									$location = get_field('location');
										if ($location) {
							?>
							<div class="location-blocks">
								<?php foreach ($location as $key => $data_row) {  
									$image = $data_row['image']['url'];
								?>
								<div class="location-blocks--item">
									<h2 class="cmn-subpage-tit"><?php echo $data_row['title'] ?></h2>
									<p class="main-img"><img src="<?php echo $image ?>" alt="<?php echo $data_row['title'] ?>"></p>
									<div class="b-ctn cmsContent">
										<?php echo $data_row['content'] ?>
									</div>
								</div>
								<?php } ?>
							</div>
							<?php } ?>
							<?php endwhile;endif; ?>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
