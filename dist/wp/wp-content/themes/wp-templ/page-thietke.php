<?php
/* Template name: Thiết kế */
$thisPageName = 'thiet-ke';
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/design.min.css">
</head>
<body id="design" class='design subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<ul class="breadcrum">
								<li><a href="<?php echo APP_ASSETS; ?>">Trang chủ</a></li>
								<li>Thiết kế</li>
							</ul>
							<div class="design-block">
								<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
								<h2 class="cmn-subpage-tit"><?php echo get_field('sub_title') ?></h2>
								<?php 
									$list = get_field('list');
									if ($list) {
								?>
								<div class="design-block-wrap">
									<?php foreach ($list as $key => $data_row) {  
											$image = $data_row['image']['url'];
									?>
									<div class="design-block-wrap--item">
										<p class="icon"><img src="<?php echo $image ?>" alt=""></p>
										<h3 class="ttl"><?php echo $data_row['title'] ?></h3>
										<div class="b-ctn cmsContent">
											<?php echo $data_row['content'] ?>
										</div>
									</div>
									<?php } ?>
								</div>
								<?php } ?>
								<?php endwhile;endif; ?>
							</div>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
