<?php
$thisPageName = 'top';
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/top.min.css">
</head>
<body id="top" class='top'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="top__mainvisual">
			<div class="container">
				<?php 
					$home_banner = get_field('home_banner', 'option');
					if($home_banner){
				?>
					<div class="slider-top">
						<?php foreach ($home_banner as $key => $data_row) {  
							$image = $data_row['image']['url'];
						?>
						<div class="main-img"><img src="<?php echo thumbCrop($image, 1170,550); ?>" alt="PROTON"></div>
						<?php } ?>
					</div>
					<?php } ?>
			</div>
		</div>
		<div class="top__intro">
			<div class="container">
				<section class="box-section">
					<div class="box-intro">
						<div class="row">
							<div class="box-intro--item box-produce col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<?php 
									$page_gioi_thieu = get_page_by_path('gioi-thieu'); 
									$img_gioi_thieu = get_the_post_thumbnail_url($page_gioi_thieu->ID);
								?>
								<h2 class="cmn-title"><?php echo $page_gioi_thieu->post_title ?></h2>
								<div class="box-article">
									<div class="box-article--item">
										<figure><a href="#"><img src="<?php echo thumbCrop($img_gioi_thieu, 552,454); ?>" alt="GIỚI THIỆU"></a></figure>
										<div class="b-info js-matcheight">
											<div class="cmn-txt">
												<p><?php echo $page_gioi_thieu->post_excerpt ?></p>
											</div>
										</div>
										<a href="<?php echo APP_URL; ?>gioi-thieu/" class="cmn-btn cmn-btn--discover">KHÁM PHÁ NGAY</a>
									</div>
								</div>
							</div>
							<div class="box-intro--item box-general col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<?php 
									$page_tong_quan = get_page_by_path('tong-quan'); 
									$img_tong_quan = get_the_post_thumbnail_url($page_tong_quan->ID);
									$sub_title_tong_quan = get_field('sub_title',$page_tong_quan->ID)
								?>
								<h2 class="cmn-title"><?php echo $page_tong_quan->post_title ?></h2>
								<div class="box-article">
									<div class="box-article--item">
										<figure><a href="#"><img src="<?php echo thumbCrop($img_tong_quan, 552,454); ?>" alt=""></a></figure>
										<div class="b-info js-matcheight">
											<h3 class="cmn-sub-tit"><?php echo $sub_title_tong_quan ?></h3>
											<div class="cmn-txt">
												<p><?php echo $page_tong_quan->post_excerpt ?></p>
											</div>
										</div>
										<a href="<?php echo APP_URL; ?>tong-quan/" class="cmn-btn cmn-btn--discover">KHÁM PHÁ NGAY</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<div class="top__location">
			<div class="container">
				<section class="box-section">
					<?php 
						$page_vi_tri = get_page_by_path('vi-tri'); 
					?>
					<h2 class="cmn-title"><?php echo $page_vi_tri->post_title ?></h2>
					<div class="box-location">
						<div class="box-article">
							<?php 
								$location = get_field('location',$page_vi_tri->ID);
									if ($location) {
							 ?>
							<div class="row">
								<?php foreach ($location as $key => $data_row) {  
									$image = $data_row['image']['url'];
								?>
								<div class="box-article--item col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<figure class="img-border"><a href="<?php echo APP_URL; ?>vi-tri/"><img src="<?php echo $image ?>" alt="<?php echo $data_row['title'] ?>"></a></figure>
									<div class="b-info">
										<h3 class="cmn-sub-tit"><?php echo $data_row['title'] ?></h3>
										<div class="cmn-txt">
											<?php echo $data_row['content'] ?>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
							<?php }	 ?>
						</div>
					</div>
				</section>
			</div>
		</div>
		<div class="top__design">
			<div class="container">
				<section class="box-section">
					<?php 
						$page_tam_nhin = get_page_by_path('tam-nhin'); 
						$img_tam_nhin = get_the_post_thumbnail_url($page_tam_nhin->ID);
						$sub_title_tam_nhin = get_field('sub_title',$page_tam_nhin->ID);
					?>
					<h2 class="cmn-title"><?php echo $page_tam_nhin->post_title ?></h2>
					<div class="inner-title">
						<h3 class="cmn-sub-tit"><?php echo $sub_title_tam_nhin ?></h3>
					</div>
					<div class="box-design">
						<?php 
							$list = get_field('list',$page_tam_nhin->ID);
							if ($list) {
						?>
						<div class="row">
							<?php foreach ($list as $key => $data_row) {  
									$image = $data_row['image']['url'];
							?>
							<div class="box-design--item col-lg-4 col-md-4 col-sm4 col-xs-12">
								<p class="img-icon"><img src="<?php echo $image ?>" alt=""></p>
								<div class="b-info js-matcheight">
									<h4 class="cmn-sub-tit"><?php echo $data_row['title'] ?></h4>
									<div class="cmn-txt">
										<?php echo $data_row['content'] ?>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<?php } ?>
					</div>
				</section>
			</div>
		</div>
		<div class="top__project">
			<div class="container">
				<section class="box-section">
					<h2 class="cmn-title">DỰ ÁN</h2>
					<div class="inner-title">
						<h3 class="cmn-sub-tit">Mang nét đẹp và hơi thở của cuộc sống hạnh phúc</h3>
					</div>
					<div class="box-project">
						<div class="box-article">
							<div class="row">
								<?php
		            	$wp_du_an = new WP_Query();
		            	$param = array(
		            		'post_type'=>'du_an',
		            		'posts_per_page' => '2',
		          		);
		          		$wp_du_an->query($param);
		          		if($wp_du_an->have_posts()):while($wp_du_an->have_posts()) : $wp_du_an->the_post();
		          			$fields = get_fields();
		          			$title = get_the_title();
		          			$image = get_the_post_thumbnail_url();
										if (!$image) {
											$image = get_first_image($post->post_content,true);
										}
		          	?>
								<div class="box-article--item col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<figure><a href="<?php echo the_permalink();?>"><img src="<?php echo thumbCrop($image, 820, 420); ?>" alt=""></a></figure>
									<div class="b-info js-matcheight">
										<h3 class="cmn-sub-tit"><?php echo $title ?></h3>
										<div class="cmn-txt">
											<?php the_excerpt() ?>
										</div>
									</div>
									<a href="<?php echo the_permalink();?>" class="cmn-btn cmn-btn--discover">KHÁM PHÁ NGAY</a>
								</div>
								<?php endwhile; endif; ?>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<div class="top__gallery">
			<div class="container">
				<section class="box-section">
					<h2 class="cmn-title">HÌNH ẢNH</h2>
					<?php
          	$wp_hinh_anh = new WP_Query();
          	$param = array(
          		'post_type'=>'hinh_anh',
          		'posts_per_page' => '1',
        		);
        		$wp_hinh_anh->query($param);
        		if($wp_hinh_anh->have_posts()):while($wp_hinh_anh->have_posts()) : $wp_hinh_anh->the_post();
        			$fields = get_fields();
        			$title = get_the_title();
        			$gallery = $fields['gallery'];
							$count = 0;
								
        	?>
					<div class="inner-title">
						<h3 class="cmn-sub-tit"><?php $title ?></h3>
					</div>
					<?php if ($gallery) { ?>
					<div class="box-gallery">
						<ul class="list-gallery">
							<?php foreach ($gallery as $key => $data_row) {  
									$count++;
									$image = $data_row['image']['url'];
							?>
							<li><a href="<?php echo APP_URL ?>hinh_anh/"><img src="<?php echo thumbCrop($image,278,210);?>" alt=""></a></li>
							<?php } ?>
						</ul>
					</div>
					<?php } ?>
					<?php endwhile; endif; ?>
				</section>
			</div>
		</div>
		<div class="top__utilities">
			<div class="container">
				<section class="box-section">
					<h2 class="cmn-title">TIN TỨC</h2>
					<div class="box-related">
						<div class="box-article">
							<div class="row">
								<?php
		            	$wp_tin_tuc = new WP_Query();
		            	$param = array(
		            		'post_type'=>'tin_tuc',
		            		'posts_per_page' => '3',
		            		'paged' => $paged
		          		);
		          		$wp_tin_tuc->query($param);
		          		if($wp_tin_tuc->have_posts()):while($wp_tin_tuc->have_posts()) : $wp_tin_tuc->the_post();
		          			$fields = get_fields();
		          			$title = get_the_title();
		          			$image = get_the_post_thumbnail_url();
										if (!$image) {
											$image = get_first_image($post->post_content,true);
										}
		          	?>
								<div class="box-article--item col-lg-4 col-md-4 col-sm-4 col-xs-12">
									<figure><a href="<?php echo the_permalink();?>"><img src="<?php echo $image ?>" alt="<?php echo $title ?>"></a></figure>
									<h3 class="cmn-sub-tit"><?php echo $title ?></h3>
									<a href="<?php echo the_permalink();?>" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
								</div>
								<?php endwhile; endif; ?>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
		<div class="top__contact">
			<div class="container">
				<?php 
					$page_lien_he = get_page_by_path('lien-he'); 
					$sub_title_lien_he = get_field('sub_title',$page_lien_he->ID);
					$address_lien_he = get_field('address',$page_lien_he->ID);
					$hot_line_lien_he = get_field('hot_line',$page_lien_he->ID);

				?>
				<h2 class="cmn-title"><?php echo $page_lien_he->post_title ?></h2>
				<div class="cmn-box-contact">
					<h3 class="cmn-sub-tit"><?php echo $sub_title_lien_he; ?></h3>
					<div class="b-address">
						<div class="b-address-item"<?php echo $address_lien_he; ?></div>
						<div class="b-address-item"><?php echo $hot_line_lien_he; ?></div>
					</div>
					<div class="box-contact-form">
						<?php echo do_shortcode('[contact-form-7 id="111" title="Contact"]') ?>						
					</div>
				</div>
			</div>
		</div>
	</main>
</div> <!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS; ?>js/lib/slick.min.js"></script>
<script>
	$('.slider-top').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		arrows: false,
		dots: false,
		fade: true,
		cssEase: 'linear',
		speed: 800,
		autoplay: true,
		autoplaySpeed: 2800,
	});
	$('.list-gallery').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		infinite: true,
		arrows: false,
		dots: false,
		speed: 1200,
		autoplay: true,
		autoplaySpeed: 4800,
		  responsive: [
			    {
			      breakpoint: 767,
			      settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2
			      }
			    }
			  ]
	});
</script>
</body>
</html>