<?php
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/utilities.min.css">
</head>
<body id="utilities" class='utilities subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<ul class="breadcrum">
								<li><a href="<?php echo APP_ASSETS; ?>">Trang chủ</a></li>
								<li>Tin tức</li>
							</ul>
							<div class="cmn-article-blocks">
								<div class="cmn-article-blocks-item">
									<?php 
										if (have_posts()) : while (have_posts()) : the_post(); 
											$image = get_the_post_thumbnail_url();
									?>
									<h2 class="cmn-subpage-tit"><?php echo get_field('sub_title') ?></h2>
									<div class="main-img"><img src="<?php echo $image; ?>" alt=""></div>
									<h3 class="cms-title"><?php the_title() ?></h3>
									<div class="b-ctn cmsContent">
										<?php the_content();?>
									</div>
									<?php endwhile; endif; ?>
								</div>
							</div>
						</div>
						<div class="content-inner content-inner-no-height" style="display: none">
							<div class="cmn-article-related">
								<div class="box-article">
									<div class="row">
										<?php
		              		$prev_post = get_previous_post();
		              		$next_post = get_next_post();
		              	?>
										<div class="box-article--item col-lg-6 col-md-6 col-sm-6 col-xs-12">
											<?php 
												if ($prev_post) { 
													$title = get_the_title($prev_post->ID);
													$thumb = get_the_post_thumbnail_url($prev_post->ID);
													if (!$thumb) {
														$thumb = get_first_image($prev_post->post_content,true);
													}
											?>
											<figure><a href="<?php echo get_permalink($prev_post->ID);?>"><img src="<?php echo $thumb ?>" alt="<?php echo $title ?>"></a></figure>
											<h3 class="ttl cmn-sub-tit"><?php echo $title ?></h3>
											<a href="<?php echo get_permalink($prev_post->ID);?>" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
											<?php } ?>
										</div>
										<div class="box-article--item col-lg-6 col-md-6 col-sm-6 col-xs-12">
											<?php 
												if ($next_post) { 
													$title = get_the_title($next_post->ID);
													$thumb = get_the_post_thumbnail_url($next_post->ID);
													if (!$thumb) {
														$thumb = get_first_image($next_post->post_content,true);
													}
											?>
											<figure><a href="<?php echo get_permalink($next_post->ID);?>"><img src="<?php echo $thumb ?>" alt="<?php echo $title ?>"></a></figure>
											<h3 class="ttl cmn-sub-tit"><?php echo $title ?></h3>
											<a href="<?php echo get_permalink($next_post->ID);?>" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="cmn-singe-pagination">
							<?php
            		$prev_post = get_previous_post();
            		$next_post = get_next_post();
            	?>
							<a href="<?php echo get_permalink($prev_post->ID);?>" class="btn btn-prev"><span>Cũ hơn</span></a>
							<a href="<?php echo APP_URL ?>tien_ich/" class="btn-backlist"><span>Quay lại danh sách</span></a>
							<a href="<?php echo get_permalink($next_post->ID);?>" class="btn btn-next"><span>Mới hơn</span></a>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
