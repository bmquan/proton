<?php
/* Template name: Sản phẩm dịch vụ */
$thisPageName = 'san-pham-dich-vu';
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/service.min.css">
</head>
<body id="service" class='service subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<ul class="breadcrum">
								<li><a href="<?php echo APP_ASSETS; ?>">Trang chủ</a></li>
								<li>Sản phẩm & dịch vụ</li>
							</ul>
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
							<?php 
								$img = get_field('image'); 
								if($img){
							?>
							<p class="main-img"><img src="<?php echo $img['url'] ?>" alt=""></p>
							<?php } ?>
							<?php 
								$link_download = get_field('download_url'); 
								if($link_download){
							?>
							<a href="<?php echo $link_download ?>" download="<?php echo $link_download ?>" class="cmn-btn cmn-btn--large">DOWNLOAD PROFILE</a>
							<?php } ?>
							<?php endwhile;endif; ?>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
