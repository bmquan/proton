<?php
$thisPageName = 'tin_tuc';
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/utilities.min.css">
</head>
<body id="utilities" class='utilities subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<ul class="breadcrum">
								<li><a href="<?php echo APP_ASSETS; ?>">Trang chủ</a></li>
								<li>Tin tức</li>
							</ul>
							<div class="box-article">
								<div class="row">
									<?php
			            	$wp_query = new WP_Query();
			            	$param = array(
			            		'post_type'=>'tin_tuc',
			            		'posts_per_page' => '9',
			            		'paged' => $paged
			          		);
			          		$wp_query->query($param);
			          		if($wp_query->have_posts()):while($wp_query->have_posts()) : $wp_query->the_post();
			          			$fields = get_fields();
			          			$title = get_the_title();
			          			$image = get_the_post_thumbnail_url();
											if (!$image) {
												$image = get_first_image($post->post_content,true);
											}
			          	?>
									<div class="box-article--item col-lg-4 col-md-4 col-sm-4 col-xs-6">
										<figure><a href="<?php echo the_permalink();?>"><img src="<?php echo $image ?>" alt="<?php echo $title ?>"></a></figure>
										<h2 class="ttl cmn-sub-tit"><?php echo $title ?></h2>
										<a href="<?php echo the_permalink();?>" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
									</div>
									<?php endwhile; endif; ?>
									
								</div>
							</div>
						</div>
						<div class="cmn-pagenavi">
							<?php if (function_exists('wp_pagenavi')) { echo wp_pagenavi(array('query' => $wp_query)); }?>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
