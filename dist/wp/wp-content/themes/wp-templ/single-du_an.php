<?php
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/project.min.css">
</head>
<body id="project" class='project subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<ul class="breadcrum">
								<li><a href="<?php echo APP_ASSETS; ?>">Trang chủ</a></li>
								<li>Dự án</li>
							</ul>
							<h2 class="cmn-subpage-tit">mang nét đẹp và hơi thở của cuộc sống hạnh phúc </h2>
							<div class="cmn-article-blocks">
								<?php 
									if (have_posts()) : while (have_posts()) : the_post(); 
										$image = get_the_post_thumbnail_url();
								?>
								<div class="cmn-article-blocks-item">
									<div class="main-img"><img src="<?php echo $image ?>" alt=""></div>
									<h3 class="cms-title"><?php the_title() ?></h3>
									<div class="b-ctn cmsContent">
										<?php the_content();?>
									</div>
								</div>
								<?php endwhile; endif; ?>
							</div>
						</div>
						<div class="cmn-singe-pagination">
							<?php
            		$prev_post = get_previous_post();
            		$next_post = get_next_post();
            	?>
							<a href="<?php echo get_permalink($prev_post->ID);?>" class="btn btn-prev"><span>Cũ hơn</span></a>
							<a href="<?php echo APP_URL ?>du_an/" class="btn-backlist"><span>Quay lại danh sách</span></a>
							<a href="<?php echo get_permalink($next_post->ID);?>" class="btn btn-next"><span>Mới hơn</span></a>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
