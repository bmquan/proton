<?php
function my_custom_hinh_anh_post_type() {
  register_post_type('hinh_anh', array (
    'labels'                  => array (
      'name'                  => __( 'Hình Ảnh' ),
      'singular_name'         => __( 'Hình Ảnh' ),
      'add_new'               => __( 'Thêm Hình Ảnh' ),
      'parent_item_colon'     => ''
    ),
    'public'                  => true,
    'rewrite'                 => true,
    'show_ui'                 => true,
    'supports'                => array ( 'title', 'revisions' ),
    'query_var'               => true,
    'menu_icon'               => 'dashicons-welcome-write-blog',
    'has_archive'             => true,
    'hierarchical'            => false,
    'menu_position'           => 5,
    'capability_type'         => 'post',
    'show_in_admin_bar'       => true,
    'publicly_queryable'      => true,
  ));
}
add_action ( 'init', 'my_custom_hinh_anh_post_type' );