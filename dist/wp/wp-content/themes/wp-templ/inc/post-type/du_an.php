<?php
function my_custom_du_an_post_type() {
  register_post_type('du_an', array (
    'labels'                  => array (
      'name'                  => __( 'Dự Án' ),
      'singular_name'         => __( 'Dự Án' ),
      'add_new'               => __( 'Thêm Dự Án' ),
      'parent_item_colon'     => ''
    ),
    'public'                  => true,
    'rewrite'                 => true,
    'show_ui'                 => true,
    'supports'                => array ( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
    'query_var'               => true,
    'menu_icon'               => 'dashicons-welcome-write-blog',
    'has_archive'             => true,
    'hierarchical'            => false,
    'menu_position'           => 5,
    'capability_type'         => 'post',
    'show_in_admin_bar'       => true,
    'publicly_queryable'      => true,
  ));
}
add_action ( 'init', 'my_custom_du_an_post_type' );