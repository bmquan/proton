<?php
function my_custom_tin_tuc_post_type() {
  register_post_type('tin_tuc', array (
    'labels'                  => array (
      'name'                  => __( 'Tin Tức' ),
      'singular_name'         => __( 'Tin Tức' ),
      'add_new'               => __( 'Thêm Tin Tức' ),
      'parent_item_colon'     => ''
    ),
    'public'                  => true,
    'rewrite'                 => true,
    'show_ui'                 => true,
    'supports'                => array ( 'title', 'editor', 'thumbnail', 'revisions' ),
    'query_var'               => true,
    'menu_icon'               => 'dashicons-welcome-write-blog',
    'has_archive'             => true,
    'hierarchical'            => false,
    'menu_position'           => 5,
    'capability_type'         => 'post',
    'show_in_admin_bar'       => true,
    'publicly_queryable'      => true,
  ));
}
add_action ( 'init', 'my_custom_tin_tuc_post_type' );