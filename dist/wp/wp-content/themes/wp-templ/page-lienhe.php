<?php
/* Template name: Liên hệ */
$thisPageName = 'lien-he';
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/contact.min.css">
</head>
<body id="contact" class='contact subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<ul class="breadcrum">
								<li><a href="<?php echo APP_ASSETS; ?>">Trang chủ</a></li>
								<li>Liên hệ</li>
							</ul>
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
							<h2 class="cmn-title">LIÊN HỆ</h2>
							<div class="cmn-box-contact">
								<h3 class="cmn-sub-tit"><?php echo get_field('sub_title') ?></h3>
								<div class="b-address">
									<div class="b-address-item"><?php echo get_field('address') ?></div>
									<div class="b-address-item"><?php echo get_field('hot_line') ?></div>
								</div>
								<div class="box-contact-form">
									<?php the_content() ?>
								</div>
							</div>
							<?php endwhile;endif; ?>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
