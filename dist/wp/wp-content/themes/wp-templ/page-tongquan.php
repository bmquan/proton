<?php
/* Template name: Tổng quan */
$thisPageName = 'tong-quan';
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/general.min.css">
</head>
<body id="general" class='general subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<ul class="breadcrum">
								<li><a href="<?php echo APP_ASSETS; ?>">Trang chủ</a></li>
								<li>Tổng quan</li>
							</ul>
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
							<h2 class="cmn-subpage-tit"><?php echo get_field('sub_title') ?></h2>
							<p class="main-img"><img src="<?php echo get_the_post_thumbnail_url() ?>" alt=""></p>
							<div class="cmsContent">
								<?php the_content() ?>
							</div>
							<?php endwhile;endif; ?>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
