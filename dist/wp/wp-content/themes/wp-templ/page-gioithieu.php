<?php
/* Template name: Giới thiệu */
$thisPageName = 'gioi-thieu';
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/produce.min.css">
</head>
<body id="produce" class='produce subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<ul class="breadcrum">
								<li><a href="<?php echo APP_ASSETS; ?>">Trang chủ</a></li>
								<li>Giới thiệu</li>
							</ul>
							<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
							<h2 class="cmn-subpage-tit"><?php the_title() ?></h2>
							<div class="cmsContent">
								<?php the_content() ?>
							</div>
							<?php endwhile;endif; ?>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
