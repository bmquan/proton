<?php
$thisPageName = 'hinh_anh';
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/lightbox.min.css">
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/gallery.min.css">
</head>
<body id="gallery" class='gallery subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<ul class="breadcrum">
								<li><a href="<?php echo APP_ASSETS; ?>">Trang chủ</a></li>
								<li>Hình ảnh</li>
							</ul>
							<?php
	            	$wp_query = new WP_Query();
	            	$param = array(
	            		'post_type'=>'hinh_anh',
	            		'posts_per_page' => '1',
	            		'paged' => $paged
	          		);
	          		$wp_query->query($param);
	          		if($wp_query->have_posts()):while($wp_query->have_posts()) : $wp_query->the_post();
	          			$fields = get_fields();
	          			$title = get_the_title();
	          			$hinh_anh = get_field('gallery');
										
	          	?>
							<h2 class="cmn-subpage-tit"><?php echo $title ?></h2>
							<?php if ($hinh_anh) { ?>
							<div class="gallery-blocks">
								<div class="grid">
									<div class="grid-sizer"></div>
									<?php foreach ($hinh_anh as $key => $data_row) {  
										$image = $data_row['image']['url'];
									?>
									<div class="grid-item"><a href="<?php echo $image ?>" data-lightbox="image"><img src="<?php echo $image ?>" alt=""></a></div>
									<?php } ?>
								</div>
							</div>
							<?php } ?>
							<?php endwhile; endif; ?>
						</div>
						<div class="cmn-pagenavi">
							<?php if (function_exists('wp_pagenavi')) { echo wp_pagenavi(array('query' => $wp_query)); }?>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS; ?>js/lib/lightbox-plus-jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js"></script>
<script>
	$(window).on('load', function(event) {
		$('.grid').isotope({
			itemSelector: '.grid-item',
			percentPosition: true,
			  masonry: {
			  	columnWidth: '.grid-item',
			  }
		});
	});
</script>
</body>
</html>
