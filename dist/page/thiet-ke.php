<?php
$thisPageName = 'thiet-ke';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/design.min.css">
</head>
<body id="design" class='design subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<div class="design-block">
								<h2 class="cmn-subpage-tit">hoàn hảo từ mọi góc độ</h2>
								<div class="design-block-wrap">
									<div class="design-block-wrap--item">
										<p class="icon"><img src="<?php echo APP_ASSETS; ?>img/cms/thietke_icon1.jpg" alt=""></p>
										<h3 class="ttl">Ý NGHĨA</h3>
										<div class="b-ctn cmsContent">
											<p><span style="color:#ee3129">>></span>Giải quyết đầu ra nông sản địa phương và khu vực;</p>
											<p><span style="color:#ee3129">>></span>Khép kín chuỗi giá trị hàng hóa dịch vụ, tăng sức mạnh cạnh tranh thị trường nội địa;</p>
											<p><span style="color:#ee3129">>></span>Phát triển kinh tế xã hội địa phương và khu vực;</p>
											<p><span style="color:#ee3129">>></span>Tạo ra di sản thương mại đặc biệt cho khu vực;</p>
											<p><span style="color:#ee3129">>></span>Góp phần phát triển hệ thống chợ đầu mối quốc gia;</p>
											<p><span style="color:#ee3129">>></span>Chợ đầu mối là công cụ chính sách công về sức khỏe quốc gia, an ninh lương thực, xây dựng thương hiệu hàng hóa, nông sản quốc gia và là động lực đặc biệt thúc đẩy phát triển KTXH địa phương.</p>
										</div>
									</div>
									<div class="design-block-wrap--item">
										<p class="icon"><img src="<?php echo APP_ASSETS; ?>img/cms/thietke_icon2.jpg" alt=""></p>
										<h3 class="ttl">TẦM NHÌN</h3>
										<div class="b-ctn cmsContent">
											<p><span style="color:#ee3129">>></span>Tầm nhìn dài hạn trên100 năm,</p>
											<p><span style="color:#ee3129">>></span>Là chợ đầu mối lớn nhất Đông Nam Á.</p>
											<p><span style="color:#ee3129">>></span>Đầu tư xây dựng phát triển chợ đầu mối trọng điểm vùng.</p>
										</div>
									</div>
									<div class="design-block-wrap--item">
										<p class="icon"><img src="<?php echo APP_ASSETS; ?>img/cms/thietke_icon3.jpg" alt=""></p>
										<h3 class="ttl">MỤC TIÊU</h3>
										<div class="b-ctn cmsContent">
											<p><span style="color:#ee3129">>></span>Xây dựng tổ hợp kinh tế thương mạii dịch vụ thế hệ mới, lấy hoạt động bán buôn làm trọng tâm;</p>
											<p><span style="color:#ee3129">>></span>Là chợ điển hình, văn minh hiện đại hàng đầu tại Việt Nam.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
