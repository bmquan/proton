<?php
$thisPageName = 'hinh-anh';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/lightbox.min.css">
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/gallery.min.css">
</head>
<body id="gallery" class='gallery subpage'>
<!-- HEADER -->
<?php //include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<h2 class="cmn-subpage-tit">Hình ảnh hoạt động của <br>công ty Proton</h2>
							<div class="gallery-blocks">
								<div class="grid">
									<div class="grid-sizer"></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img1_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img1_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img2_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img2_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img3_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img3_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img4_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img4_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img5_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img5_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img6_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img6_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img7_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img7_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img8_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img8_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img9_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img9_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img10_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img10_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img11_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img11_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img12_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img12_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img13_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img13_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img14_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img14_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img15_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img15_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img16_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img16_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img17_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img17_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img18_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img18_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img19_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img19_large.jpg" alt=""></a></div>
									<div class="grid-item"><a href="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img20_large.jpg" data-lightbox="image-1"><img src="<?php echo APP_ASSETS; ?>img/cms/hinhanh_img20_large.jpg" alt=""></a></div>
								</div>
							</div>
						</div>
						<div class="cmn-pagenavi">
							<span class="pages">1／10 pages</span>
							<a href="" class="previouspostslink">&nbsp;</a>
							<span class="current">1</span>
							<a href="" class="page">2</a>
							<a href="" class="page">3</a>
							<span class="extend">...</span>
							<a href="" class="page">10</a>
							<a href="" class="nextpostslink">&nbsp;</a>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS; ?>js/lib/lightbox-plus-jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js"></script>
<script>
	$(window).on('load', function(event) {
		$('.grid').isotope({
			itemSelector: '.grid-item',
			percentPosition: true,
			  masonry: {
			  	columnWidth: '.grid-item',
			  }
		});
	});
</script>
</body>
</html>
