<?php
$thisPageName = 'tien-ich';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/utilities.min.css">
</head>
<body id="utilities" class='utilities subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<div class="cmn-article-blocks">
								<div class="cmn-article-blocks-item">
									<h2 class="cmn-subpage-tit">Cho cuộc sống viên mãn dài lâu</h2>
									<div class="main-img"><img src="<?php echo APP_ASSETS; ?>img/cms/tienich_img.jpg" alt=""></div>
									<h3 class="cms-title">Celadon City</h3>
									<div class="b-ctn cmsContent">
										<p>Tổng hòa những giá trị sống mà một khu đô thị đích thực hướng tới, từ sinh hoạt, giải trí cho đến sức khỏe, giáo dục.</p>
										<p>Chọn phát triển hệ thống tiện ích nội khu trước khi xây nên những ngôi nhà kiên cố, Celadon City là chốn an cư lý tưởng cho cộng đồng cư dân văn minh đang khát khao chất lượng sống đỉnh cao.</p>
										<p>Chọn phát triển hệ thống tiện ích nội khu trước khi xây nên những ngôi nhà kiên cố, Celadon City là chốn an cư lý tưởng cho cộng đồng cư dân văn minh đang khát khao chất lượng sống đỉnh cao. Chọn phát triển hệ thống tiện ích nội khu trước khi xây nên những ngôi nhà kiên cố, Celadon City là chốn an cư lý tưởng cho cộng đồng cư dân văn minh đang khát khao chất lượng sống đỉnh cao.</p>
										<p>Chọn phát triển hệ thống tiện ích nội khu trước khi xây nên những ngôi nhà kiên cố, Celadon City là chốn an cư lý tưởng cho cộng đồng cư dân văn minh đang khát khao chất lượng sống đỉnh cao. Chọn phát triển hệ thống tiện ích nội khu trước khi xây nên những ngôi nhà kiên cố, Celadon City là chốn an cư lý tưởng cho cộng đồng cư dân văn minh đang khát khao chất lượng sống đỉnh cao. Chọn phát triển hệ thống tiện ích nội khu trước khi xây nên những ngôi nhà kiên cố, Celadon City là chốn an cư lý tưởng cho cộng đồng cư dân văn minh đang khát khao chất lượng sống đỉnh cao.</p>
										<p>Chọn phát triển hệ thống tiện ích nội khu trước khi xây nên những ngôi nhà kiên cố, Celadon City là chốn an cư lý tưởng cho cộng đồng cư dân văn minh đang khát khao chất lượng sống đỉnh cao. Chọn phát triển hệ thống tiện ích nội khu trước khi xây nên những ngôi nhà kiên cố, Celadon City là chốn an cư lý tưởng cho cộng đồng cư dân văn minh đang khát khao chất lượng sống đỉnh cao.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="content-inner content-inner-no-height">
							<div class="cmn-article-related">
								<div class="box-article">
									<div class="row">
										<div class="box-article--item col-lg-6 col-md-6 col-sm-6 col-xs-12">
											<figure><a href="#"><img src="<?php echo APP_ASSETS; ?>img/top/tienich_img.jpg" alt=""></a></figure>
											<a href="<?php echo APP_URL; ?>tien-ich/" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
										</div>
										<div class="box-article--item col-lg-6 col-md-6 col-sm-6 col-xs-12">
											<figure><a href="#"><img src="<?php echo APP_ASSETS; ?>img/top/tienich_img.jpg" alt=""></a></figure>
											<a href="<?php echo APP_URL; ?>tien-ich/" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
