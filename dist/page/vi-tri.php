<?php
$thisPageName = 'vi-tri';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/location.min.css">
</head>
<body id="location" class='location subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<div class="location-blocks">
								<div class="location-blocks--item">
									<h2 class="cmn-subpage-tit">chợ đầu mối sóc trăng</h2>
									<p class="main-img"><img src="<?php echo APP_ASSETS; ?>img/cms/vitri_img1.jpg" alt=""></p>
									<div class="b-ctn cmsContent">
										<p>1. Nằm tiếp giáp cửa ngõ Biển Đông, Sóc Trăng có vị trí địa chiến lược tại khu vực Đông Nam Á và thế giới.</p>
										<p>2. Nằm ngay hạ lưu sông Hậu, Sóc Trăng sẽ có vai trò là cảng biển cửa ngõ quốc tế với bến cảng chính là bến cảng ngoài khơi cửa Trần Đề, đáp ứng cho tàu trọng tải 50.000 – 100.000 DWT và trên 100.000 DWT, vì vậy, Sóc Trăng sẽ là nơi tốt nhất để tập kết hàng hóa nông sản của toàn vùng ĐBSCL rộng lớn đến chợ đầu mối quốc tế Mekong để xuất đi các nước.</p>
									</div>
								</div>
								<div class="location-blocks--item">
									<h2 class="cmn-subpage-tit">đồng nai</h2>
									<p class="main-img"><img src="<?php echo APP_ASSETS; ?>img/cms/vitri_img2.jpg" alt=""></p>
									<div class="b-ctn cmsContent">
										<p>Nằm tại Trung tâm Quận Tân Phú – cửa ngõ khu Tây, kết nối thuận tiện các vị trí trọng điểm nội thành (Cơ Sở Ban Ngành, Tuyến Metro số 2, Sân Bay Tân Sơn Nhất) và gắn liền các tuyến giao thông huyết mạch dẫn đến các tỉnh Đông Tây.</p>
										<p><strong>Celadon City</strong><br/>Là minh chứng rõ nét thể hiện sự tinh tế trong tầm nhìn, tâm huyết và sự chỉn chu trong cam kết của nhà đầu tư nhằm mang lại một cuộc sống tiện nghi bậc nhất cho cộng đồng cư dân văn minh năng động.</p>
									</div>
								</div>
							</div>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
