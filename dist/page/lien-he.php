<?php
$thisPageName = 'lien-he';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/contact.min.css">
</head>
<body id="contact" class='contact subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<h2 class="cmn-title">LIÊN HỆ</h2>
							<div class="cmn-box-contact">
								<h3 class="cmn-sub-tit">Đăng ký ngay để nhận thông tin và ưu đãi mới nhất</h3>
								<div class="b-address">
									<div class="b-address-item">ĐỊA CHỈ<br />Công ty TNHH Mtv Proton<br />46 Lê Đức Thọ, P. 7, Q. Gò Vấp, TP. HCM</div>
									<div class="b-address-item">HOTLINE<br />02838 953 368</div>
								</div>
								<div class="box-contact-form">
									<form action="/" method="post" class="wpcf7-form" novalidate="novalidate">
										<div class="row">
											<div class="box-contact-form-item col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="form-group"><input type="text" name="nameuser" placeholder="HỌ TÊN"></div>
												<div class="form-group"><input type="tel" name="tel" placeholder="Điện thoại"></div>
												<div class="form-group"><input type="email" name="email" placeholder="Email"></div>
											</div>
											<div class="box-contact-form-item col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<div class="form-group"><textarea placeholder="Nội dung tin nhắn"></textarea></div>
											</div>
										</div>
										<button type="submit">GỬI</button>
									</form>
								</div>
							</div>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
