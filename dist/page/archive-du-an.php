<?php
$thisPageName = 'du-an';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/project.min.css">
</head>
<body id="project" class='project subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<div class="box-article">
								<div class="row">
									<div class="box-article--item col-lg-4 col-md-4 col-sm-4 col-xs-6">
										<figure><a href="#"><img src="<?php echo APP_ASSETS; ?>img/top/tienich_img.jpg" alt=""></a></figure>
										<h2 class="ttl cmn-sub-tit">Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết</h2>
										<a href="<?php echo APP_URL; ?>tien-ich/" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
									</div>
									<div class="box-article--item col-lg-4 col-md-4 col-sm-4 col-xs-6">
										<figure><a href="#"><img src="<?php echo APP_ASSETS; ?>img/top/tienich_img.jpg" alt=""></a></figure>
										<h2 class="ttl cmn-sub-tit">Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết</h2>
										<a href="<?php echo APP_URL; ?>tien-ich/" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
									</div>
									<div class="box-article--item col-lg-4 col-md-4 col-sm-4 col-xs-6">
										<figure><a href="#"><img src="<?php echo APP_ASSETS; ?>img/top/tienich_img.jpg" alt=""></a></figure>
										<h2 class="ttl cmn-sub-tit">Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết</h2>
										<a href="<?php echo APP_URL; ?>tien-ich/" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
									</div>
									<div class="box-article--item col-lg-4 col-md-4 col-sm-4 col-xs-6">
										<figure><a href="#"><img src="<?php echo APP_ASSETS; ?>img/top/tienich_img.jpg" alt=""></a></figure>
										<h2 class="ttl cmn-sub-tit">Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết</h2>
										<a href="<?php echo APP_URL; ?>tien-ich/" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
									</div>
									<div class="box-article--item col-lg-4 col-md-4 col-sm-4 col-xs-6">
										<figure><a href="#"><img src="<?php echo APP_ASSETS; ?>img/top/tienich_img.jpg" alt=""></a></figure>
										<h2 class="ttl cmn-sub-tit">Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết</h2>
										<a href="<?php echo APP_URL; ?>tien-ich/" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
									</div>
									<div class="box-article--item col-lg-4 col-md-4 col-sm-4 col-xs-6">
										<figure><a href="#"><img src="<?php echo APP_ASSETS; ?>img/top/tienich_img.jpg" alt=""></a></figure>
										<h2 class="ttl cmn-sub-tit">Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết</h2>
										<a href="<?php echo APP_URL; ?>tien-ich/" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
									</div>
									<div class="box-article--item col-lg-4 col-md-4 col-sm-4 col-xs-6">
										<figure><a href="#"><img src="<?php echo APP_ASSETS; ?>img/top/tienich_img.jpg" alt=""></a></figure>
										<h2 class="ttl cmn-sub-tit">Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết</h2>
										<a href="<?php echo APP_URL; ?>tien-ich/" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
									</div>
									<div class="box-article--item col-lg-4 col-md-4 col-sm-4 col-xs-6">
										<figure><a href="#"><img src="<?php echo APP_ASSETS; ?>img/top/tienich_img.jpg" alt=""></a></figure>
										<h2 class="ttl cmn-sub-tit">Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết</h2>
										<a href="<?php echo APP_URL; ?>tien-ich/" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
									</div>
									<div class="box-article--item col-lg-4 col-md-4 col-sm-4 col-xs-6">
										<figure><a href="#"><img src="<?php echo APP_ASSETS; ?>img/top/tienich_img.jpg" alt=""></a></figure>
										<h2 class="ttl cmn-sub-tit">Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết Tiêu đề bài viết</h2>
										<a href="<?php echo APP_URL; ?>tien-ich/" class="cmn-btn cmn-btn--detail">CHI TIẾT</a>
									</div>
								</div>
							</div>
						</div>
						<div class="cmn-pagenavi">
							<span class="pages">1／10 pages</span>
							<a href="" class="previouspostslink">&nbsp;</a>
							<span class="current">1</span>
							<a href="" class="page">2</a>
							<a href="" class="page">3</a>
							<span class="extend">...</span>
							<a href="" class="page">10</a>
							<a href="" class="nextpostslink">&nbsp;</a>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
