<?php
$thisPageName = 'gioi-thieu';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/produce.min.css">
</head>
<body id="produce" class='produce subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<ul class="breadcrum">
								<li><a href="<?php echo APP_ASSETS; ?>">Trang chủ</a></li>
								<li>Giới thiệu</li>
							</ul>
							<h2 class="cmn-subpage-tit">GIỚI THIỆU</h2>
							<div class="cmsContent">
								<p>Công ty Proton là đơn vị chuyên đầu tư phát triển chợ đầu mối tại Việt Nam. Với sứ mệnh kết nối và tạo dựng chuỗi giá trị nông sản phục vụ người tiêu dùng, chúng tôi đang từng bước xây dựng chuỗi chợ đầu mối theo mô hình mới tại Việt Nam.</p>
								<p>Đây là mô hình chợ đầu mối gắn kết: Nhà vườn, doanh nghiệp chế biến, xuất nhập khẩu, kết hợp bán lẻ và tham quan du lịch. Chuỗi chợ đầu mối Proton dự kiến sẽ được xây dựng tại các tỉnh thuận lợi về giao thương và liên kết vùng như: Đồng Nai, Long An, Sóc Trăng, Đà Nẵng.</p>
								<p>Ngoài chợ Đầu mối Dầu Giây - Đồng Nai mà Công ty Proton đã tham gia thực hiện, dự kiến đến năm 2020 chúng tôi sẽ có thêm 2 đến 3 chợ đầu mối mới.</p>
								<span class="img-wrap"><img src="<?php echo APP_ASSETS; ?>img/cms/produce_img.png" alt=""></span>
								<p>Công ty Proton đang có các đối tác là thương nhân chợ đầu mối lớn, các trang trại, vùng trồng lớn, cùng nhiều đơn vị cung cấp dịch vụ đặc biệt cho chợ đầu mối, vì vậy chúng tôi có thể triển khai nhanh, hiệu quả các dự án chợ đầu mối tại Việt Nam hiện nay.</p>
								<p>Chúng tôi sẵn sàng hợp tác với các tổ chức, cá nhân trong và ngoài nước để xây dựng chuỗi chợ đầu mối và các dịch vụ phụ trợ cho chợ, góp phần nâng cao chất lượng sản phẩm và dịch vụ tại chợ đầu mối, mang lại nhiều lợi ích cho người tiêu dùng, thúc đẩy phát triển kinh tế nông nghiệp bền vững.</p>
							</div>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
