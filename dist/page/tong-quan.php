<?php
$thisPageName = 'tong-quan';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/general.min.css">
</head>
<body id="general" class='general subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<h2 class="cmn-subpage-tit">Proton đang đầu tư phát triển <br>chuỗi giá trị nông nghiệp <br>công nghệ cao gồm:</h2>
							<p class="main-img"><img src="<?php echo APP_ASSETS; ?>img/cms/general_img.jpg" alt=""></p>
							<div class="cmsContent">
								
								<p>1. Nghiên cứu, ứng dụng công nghệ cao cho nông nghiệp, hợp tác và chuyển giao mô hình trang trại mẫu nông nghiệp công nghệ cao 4.0<br />
									2. Hướng dẫn thu hoạch, bảo quản, đóng gói và bao tiêu đầu ra sản phẩm<br />
									3. Thu mua nông sản để phục vụ xuất khẩu, phân phối cho Chuỗi chợ đầu mối nông sản, siêu thị, trạm dừng chân và khách du lịch<br />
									4. Phát triển chuỗi điểm đến du lịch nông nghiệp (farm tour), huấn luyện kỹ năng<br />
									5. Xây dựng trung tâm hội chợ, triển lãm và chợ phiên nông sản thúc đẩy quảng bá nông sản Việt Nam<br />
									6. Xây dựng và phát triển chợ đầu mối</p>
							</div>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
