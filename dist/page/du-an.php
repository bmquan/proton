<?php
$thisPageName = 'du-an';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/project.min.css">
</head>
<body id="project" class='project subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<ul class="breadcrum">
								<li><a href="<?php echo APP_ASSETS; ?>">Trang chủ</a></li>
								<li>Dự án</li>
							</ul>
							<h2 class="cmn-subpage-tit">mang nét đẹp và hơi thở của cuộc sống hạnh phúc </h2>
							<div class="cmn-article-blocks">
								<div class="cmn-article-blocks-item">
									<div class="main-img"><img src="<?php echo APP_ASSETS; ?>img/cms/duan_img1.jpg" alt=""></div>
									<h3 class="cms-title">Chợ đầu mối nông sản thực phẩm Dầu Giây</h3>
									<div class="b-ctn cmsContent">
										<p>Chợ đầu mối nông sản thực phẩm Dầu Giây do Công ty CP Bất động sản Thống Nhất (thuộc Tổng công ty Tín Nghĩa) được Tỉnh ủy, UBND tỉnh Đồng Nai giao làm chủ đầu tư và Công ty TNHH MTV Proton được giao khai thác phát triển chợ.</p>
										<p>Tọa lạc gần ngã ba Dầu Giây (xã Xuân Thạnh, huyện Thống Nhất), chợ kết nối được cả trục lộ trọng điểm gồm QL 20, QL 1 và Cao tốc Long Thành – Dầu Giây, thuận tiện cho việc chuyên chở hàng hóa nông sản thực phẩm từ các địa phương trong khu vực tới đây như Lâm Đồng, Ninh Thuận, Bình Thuận, miền Trung, miền Tây… đặc biệt là các địa phương của tỉnh Đồng Nai.</p>
										<p>Hiện cơ sở hạ tầng chợ đã hoàn thiện, 216 ô, vựa đã được hoàn tất và 317 nhà phố chợ (diện tích từ 16 - 32m2, có thời gian sở hữu, sử dụng là 50 năm) sẽ tiếp tục được hoàn tất.</p>
										<p>Với diện tích 7ha, chợ tập trung kinh doanh các mặt hàng rau, củ, quả, trái cây tươi.</p>
										<p>Trước khi chợ đi vào hoạt động, ngày 19-5, Sở Công Thương, Sở Nông nghiệp và Phát triển Nông thôn, Trung tâm Xúc tiến Thương mại tỉnh Đồng Nai đã cùng Công ty CP bất động sản Thống Nhất và Công ty TNHH MTV Proton tổ chức chương trình giao lưu kết nối giữa các hợp tác xã, tổ hợp tác, nhà vườn và thương nhân, chủ vựa… giao dịch mua bán thông qua chợ đầu mối nông sản thực phẩm Dầu Giây. Ban quản lý chợ cho biết các cơ sở sản xuất nông sản thực phẩm có nhu cầu đưa hàng hóa vào chợ liên hệ trực tiếp với Ban quản lý để được hướng dẫn, hỗ trợ các thủ tục.</p>
										<p>Theo ông Nguyễn Hồng Long, Tổng Giám đốc Công ty TNHH MTV Proton, các hợp tác xã, nhà vườn mà Ban quản lý Chợ kết nối đều là những đối tác có uy tín, ý thức được sự cần thiết phải sản xuất nguồn hàng đạt tiêu chuẩn để cung cấp cho chợ. Tuy đi vào hoạt động, lượng hàng hóa giao dịch chưa nhiều nhưng bà con rất hào hứng đưa những sản phẩm được tuyển chọn kỹ, có chất lượng cao ra bán trong ngày khai trương.</p>
										<p>Được biết, sau khi đi vào hoạt động ổn định, chợ đầu mối nông sản thực phẩm Dầu Giây sẽ tiếp tục xây dựng giai đoạn II theo phương án mở rộng thêm hơn 27 ha, có khu bán thực phẩm sạch, gồm thịt heo, gà và các loại thủy, hải sản.</p>
									</div>
								</div>
								<div class="cmn-article-blocks-item">
									<div class="main-img"><img src="<?php echo APP_ASSETS; ?>img/cms/duan_img2.jpg" alt=""></div>
									<h3 class="cms-title">Chợ đầu mối quốc tế Mekong khu vực DBSCL</h3>
									<div class="b-ctn cmsContent">
										<p>Dự án Chợ đầu mối quốc tế Mekong khu vực ĐBSCL tại Sóc Trăng là tổ hợp kinh tế nền tảng liên ngành, lấy hoạt động chợ đầu mối làm trung tâm, kết hợp phát triển công nghiệp chế biến sâu, dịch vụ logistics, vui chơi giải trí và du lịch, đây là mô hình đặc biệt được đề xuất đầu tiên tại Việt Nam;</p>
										<p>Dự án khép kín chuỗi giá trị: Đầu vào-đầu ra và dịch vụ phụ trợ với hệ sinh thái thương mại hiện đại, tối ưu tiện ích hạ tầng, tạo ra môi trường sinh sống lý tưởng: Ở - Sản xuất - Kinh doanh thương mại, dịch vụ - Vui chơi giải trí và làm du lịch.</p>
										<p>Hoạt động chính: Thương mại, dịch vụ, sản xuất, chế biến nông lâm thủy hải sản.</p>
									</div>
								</div>
							</div>
						</div>
						<div class="cmn-singe-pagination">
							<a href="#" class="btn btn-prev"><span>Cũ hơn</span></a>
							<a href="#" class="btn-backlist"><span>Quay lại danh sách</span></a>
							<a href="#" class="btn btn-next"><span>Mới hơn</span></a>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
