<?php
$thisPageName = 'san-pham-dich-vu';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/service.min.css">
</head>
<body id="service" class='service subpage'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="container">
			<div class="subpage-block">
				<div class="row">
					<div class="subpage-block--content col-lg-9 col-md-9 col-sm-9 col-xs-12">
						<div class="content-inner">
							<p class="main-img"><img src="<?php echo APP_ASSETS; ?>img/cms/dichvu_img.jpg" alt=""></p>
							<a href="file_download.txt" download="file_download.txt" class="cmn-btn cmn-btn--large">DOWNLOAD PROFILE</a>
						</div>
					</div>

					<?php include(APP_PATH.'libs/sidebar.php'); ?>
				</div>
			</div>
		</div>
	</main>
</div><!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
</body>
</html>
