<?php
$thisPageName = 'top';
include_once(dirname(__DIR__) . '/app_config.php');
include(APP_PATH.'libs/head.php');
?>
<link rel="stylesheet" href="<?php echo APP_ASSETS ?>css/page/top.min.css">
</head>
<body id="top" class='top'>
<!-- HEADER -->
<?php include(APP_PATH.'libs/header.php'); ?>
<div id="wrap">
	<main>
		<div class="top__mainvisual">
			<div class="container">
				<div class="slider-top">
					<div class="main-img"><img src="<?php echo APP_ASSETS; ?>img/top/main_visual.jpg" alt="PROTON"></div>
					<div class="main-img"><img src="<?php echo APP_ASSETS; ?>img/top/main_visual.jpg" alt="PROTON"></div>
					<div class="main-img"><img src="<?php echo APP_ASSETS; ?>img/top/main_visual.jpg" alt="PROTON"></div>
				</div>
			</div>
		</div>
	</main>
</div> <!-- #wrap -->
<!-- FOOTER -->
<?php include(APP_PATH.'libs/footer.php'); ?>
<script src="<?php echo APP_ASSETS; ?>js/lib/slick.min.js"></script>
<script>
	$('.slider-top').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		arrows: false,
		dots: false,
		fade: true,
		cssEase: 'linear',
		speed: 800,
		autoplay: true,
		autoplaySpeed: 2800,
	});
</script>
</body>
</html>