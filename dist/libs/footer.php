<footer class="footer">
	<div class="container">
		<?php 
			$footer = get_field('footer_text', 'option');
			echo $footer;
		 ?>
	</div>
</footer>

<script src="<?php echo APP_ASSETS; ?>js/lib/jquery1-12-4.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/lib/smoothscroll.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/lib/jquery.matchHeight.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/lib/modernizr.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/common.min.js"></script>
<script src="<?php echo APP_ASSETS; ?>js/functions.min.js"></script>
<script>
	$('.js-matcheight').matchHeight();
</script>
