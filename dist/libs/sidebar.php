<div class="subpage-block--sidebar col-lg-3 col-md-3 col-sm-3 col-xs-12">
	<div class="list-sidebar">
		<div class="list-sidebar--item">
			<div class="ttl">SOCIAL PROFILE</div>
			<ul class="social-list">
				<?php 
					$twitter = get_field('twitter_url', 'option');
					if($twitter){
				?>
				<li><a href="<?php echo $twitter ?>"><img src="<?php echo APP_ASSETS; ?>img/common/icon/social_tw.png" alt=""></a></li>
				<?php } ?>
				<?php 
					$facebook = get_field('facebook_url_2', 'option');
					if($facebook){
				?>
				<li><a href="<?php echo $facebook ?>"><img src="<?php echo APP_ASSETS; ?>img/common/icon/social_fb.png" alt=""></a></li>
				<?php } ?>
				<?php 
					$google_plus = get_field('google_plus_url', 'option');
					if($google_plus){
				?>
				<li><a href="<?php echo $google_plus ?>"><img src="<?php echo APP_ASSETS; ?>img/common/icon/social_gplus.png" alt=""></a></li>
				<?php } ?>
				<?php 
					$linkedin = get_field('linkedin_url', 'option');
					if($linkedin){
				?>
				<li><a href="<?php echo $linkedin ?>"><img src="<?php echo APP_ASSETS; ?>img/common/icon/social_linked.png" alt=""></a></li>
				<?php } ?>
				<?php 
					$mail = get_field('mail_url', 'option');
					if($mail){
				?>
				<li><a href="<?php echo $mail ?>"><img src="<?php echo APP_ASSETS; ?>img/common/icon/social_mail.png" alt=""></a></li>
				<?php } ?>
			</ul>
		</div>
		<?php wp_nav_menu(); ?>
	</div>					
</div>