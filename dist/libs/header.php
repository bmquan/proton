<header class="header">
	<div class="container">
		<div class="header-info">
			<div class="header__logo">
				<?php 
					$logo = get_field('logo', 'option');
					$logo = $logo['url'];
					if($logo == ''){
						$logo = APP_ASSETS.'img/common/logo.png';
					}
				?>
				<p class="logo"><a href="<?php echo APP_URL; ?>"><img src="<?php echo $logo ?>" width="218" alt="PROTON Wholesale Market Developer"></a></p>
			</div>
			<div class="header__social">
				<ul class="list-social">
					<?php 
						$tel = get_field('tel_url', 'option');
						if($tel){
					 ?>
					<li><a href="<?php echo $tel ?>"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_tel.png" width="31" alt="tel"></a></li>
					<?php } ?>
					<?php 
						$access = get_field('access_url', 'option');
						if($access){
					 ?>
					<li><a href="<?php echo $access ?>"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_mail.png" width="31" alt="mail"></a></li>
					<?php } ?>
					<?php 
						$facebook = get_field('facebook_url', 'option');
						if($facebook){
					 ?>
					<li><a href="<?php echo $facebook ?>"><img src="<?php echo APP_ASSETS; ?>img/common/icon/ico_fb.png" width="31" alt="facebook"></a></li>
					<?php } ?>
				</ul>
			</div>
			<div class="header__gnavi">
				<nav>
					<?php wp_nav_menu(); ?>
				</nav>
			</div>
			<div class="menu-btn">
        <div class="burger-icon"></div>
			</div>
		</div>
	</div>
</header>